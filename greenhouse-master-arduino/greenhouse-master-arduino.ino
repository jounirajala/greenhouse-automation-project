#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

RF24 radio(9,10);
const uint64_t pipes[2] = { 0xEEFDFDFDECLL, 0xEEFDFDF0DFLL };

void setup() {
  Serial.begin(9600);
  printf_begin();
  radio.begin();
  radio.startListening();
  radio.stopListening();
  radio.setChannel(10);
  radio.setCRCLength( RF24_CRC_16 ) ;
  radio.enableDynamicPayloads();
  radio.setAutoAck( true ) ;
  radio.setPALevel( RF24_PA_HIGH ) ;
  radio.setRetries(5,15);
  radio.printDetails();
  radio.openWritingPipe(pipes[1]);
  radio.openReadingPipe(1,pipes[0]);
}


void loop() {
    radio.startListening();
    if ( radio.available() ) {
      uint8_t len = radio.getDynamicPayloadSize() + 1;
      char payload[len];
      
      radio.read( &payload, len );
      Serial.println(String(payload));
    }
    delay(1000);
}


