var influx = require('influx');
var fs = require('fs');
var nconf = require('nconf');

nconf.argv().env().file({ file: './config.json' });

nconf.file("foobar.conf").save(function (err) {
    if (err) {
      console.error(err.message);
      return;
    }
    console.log('Configuration saved successfully.');
});

var influxdbHost = nconf.get('influxdbHost');
var influxdbPort = nconf.get('influxdbPort');
var influxdbUsername = nconf.get('influxdbUser');
var influxdbPassword = nconf.get('influxdbPassword');
var influxdbDatabase = nconf.get('influxdbDatabase');
var serialPortName = nconf.get('serialPort');

console.log('Setting up...');
console.log(' InfluxDB URL: http://'+influxdbHost+":"+influxdbPort+"/db/"+influxdbDatabase);
console.log(' User/password '+influxdbUsername+":"+influxdbPassword);
console.log(' Serial Port: '+serialPortName);

var serverInflux = influx();
var client = influx({
  host : influxdbHost,
  port : influxdbPort,
  username : influxdbUsername,
  password : influxdbPassword,
  database : influxdbDatabase
});

var serialport = require("serialport");
var arduino = new serialport.SerialPort(serialPortName, {
  baudrate: 9600,
  databits: 8,
  stopbits: 1,
  parity: 'none',
  openImmediately: false,
  parser: serialport.parsers.readline("\n")
});

arduino.on("open", function() {
  console.log("Opening Serial Communications with Arduino via Serial Port");

  arduino.on('data', function(data) {
    var arr = data.split(':');
    var cmd = arr[0];
    var args = arr.slice(1);
    switch(cmd) {
      case 'AT' :
        console.log('Ambient Temperature is ' + args[0]);
	      client.writePoint('greenhouse.ambient.temperature', {date: new Date(), value: parseFloat(args[0])}, influxError);
        break;
      case 'AL' :
        console.log('Ambient Lightning is ' + args[0]);
	      client.writePoint('greenhouse.ambient.lightning', {date: new Date(), value: parseInt(args[0])}, influxError);
        break;
      case 'PM1' :
        console.log('Pot 1 Moisture is ' + args[0]);
	      client.writePoint('greenhouse.pot.1.moisture', {date: new Date(), value: parseInt(args[0])}, influxError);
        break;
      case 'PM2' :
        console.log('Pot 2 Moisture is ' + args[0]);
	      client.writePoint('greenhouse.pot.2.moisture', {date: new Date(), value: parseInt(args[0])}, influxError);
        break;
      case 'PM3' :
        console.log('Pot 3 Moisture is ' + args[0]);
	      client.writePoint('greenhouse.pot.3.moisture', {date: new Date(), value: parseInt(args[0])}, influxError);
        break;
      case 'PM4' :
        console.log('Pot 4 Moisture is ' + args[0]);
	      client.writePoint('greenhouse.pot.4.moisture', {date: new Date(), value: parseInt(args[0])}, influxError);
        break;
      case 'PT' :
        console.log('Pot Temperature is ' + args[0]);
	      client.writePoint('greenhouse.pot.temperature', {date: new Date(), value: parseFloat(args[0])}, influxError);
        break;
      default :
        console.log('Unknown Command');
    }
  });

});

arduino.on('error', function(error) {
  console.log("Error with serial port communications with Arduino" + error);
});


function influxError(err, response) {
// console.log('Error while sending sensor metric to influxdb'+response);
}
