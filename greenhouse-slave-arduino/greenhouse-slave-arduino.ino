
#include <OneWire.h>
#include <DallasTemperature.h>
#include <SPI.h>
#include "nRF24L01.h"
#include "RF24.h"
#include "printf.h"

#define ONE_WIRE_BUS 2
#define POT_MOISTURE_1_VCC 3
#define POT_MOISTURE_1 A0
#define POT_MOISTURE_2_VCC 4
#define POT_MOISTURE_2 A1
#define POT_MOISTURE_3_VCC 5
#define POT_MOISTURE_3 A2
#define POT_MOISTURE_4_VCC 6
#define POT_MOISTURE_4 A3
#define AMBIENT_LIGHT_VCC 7
#define AMBIENT_LIGHT A4

OneWire ourWire(ONE_WIRE_BUS);
DallasTemperature sensors(&ourWire);

DeviceAddress AmbientProbe = { 0x28, 0xFF, 0x4E, 0x66, 0x53, 0x15, 0x02, 0xAA }; 
DeviceAddress PotProbe = { 0x28, 0xFF, 0xD1, 0x5F, 0x53, 0x15, 0x02, 0xA9 }; 

RF24 radio(9,10);
const uint64_t pipes[2] = { 0xEEFDFDFDECLL, 0xEEFDFDF0DFLL };

void setup() {
  Serial.begin(9600);
  printf_begin();

  pinMode(POT_MOISTURE_1, INPUT);
  pinMode(POT_MOISTURE_1_VCC, OUTPUT);
  digitalWrite(POT_MOISTURE_1_VCC, LOW);

  pinMode(POT_MOISTURE_2, INPUT);
  pinMode(POT_MOISTURE_2_VCC, OUTPUT);
  digitalWrite(POT_MOISTURE_2_VCC, LOW);

  pinMode(POT_MOISTURE_3, INPUT);
  pinMode(POT_MOISTURE_3_VCC, OUTPUT);
  digitalWrite(POT_MOISTURE_3_VCC, LOW);

  pinMode(POT_MOISTURE_4, INPUT);
  pinMode(POT_MOISTURE_4_VCC, OUTPUT);
  digitalWrite(POT_MOISTURE_4_VCC, LOW);

  pinMode(AMBIENT_LIGHT, INPUT);
  pinMode(AMBIENT_LIGHT_VCC, OUTPUT);
  digitalWrite(AMBIENT_LIGHT_VCC, LOW);

  sensors.setResolution(TEMP_12_BIT);
  sensors.begin();

  radio.begin();
  radio.startListening();
  radio.stopListening();
  radio.setChannel(10);
  radio.setCRCLength( RF24_CRC_16 ) ;
  radio.enableDynamicPayloads();
  radio.setAutoAck( true ) ;
  radio.setPALevel( RF24_PA_HIGH ) ;
  radio.setRetries( 10,50 );
  radio.printDetails();
  radio.openWritingPipe(pipes[0]);
  radio.openReadingPipe(1,pipes[1]);
}


void loop() {
    sendToMaster("PM1:"+String(getSensorValue(POT_MOISTURE_1_VCC, POT_MOISTURE_1, true )));
    sendToMaster("PM2:"+String(getSensorValue(POT_MOISTURE_2_VCC, POT_MOISTURE_2, true )));
    sendToMaster("PM3:"+String(getSensorValue(POT_MOISTURE_3_VCC, POT_MOISTURE_3, true )));
    sendToMaster("PM4:"+String(getSensorValue(POT_MOISTURE_4_VCC, POT_MOISTURE_4, true )));
    sendToMaster("AL:"+String(getSensorValue(AMBIENT_LIGHT_VCC, AMBIENT_LIGHT, false )));
    sensors.requestTemperatures();
    sendToMaster("PT:"+String( sensors.getTempC(PotProbe)));
    sendToMaster("AT:"+String( sensors.getTempC(AmbientProbe)));
    delay(10000);
}

void sendToMaster(String message) {
    int len = message.length() +1;
    char buffer[len];
    message.toCharArray(buffer, len);
    radio.write( &buffer, len, false );
    Serial.println(message);
    delay(1000);
}

int getSensorValue(int vcc, int data, bool negate) {
  int retVal=0;
  digitalWrite(vcc, HIGH);
  delay(50);
  for(int i = 0; i < 5; i++) {
    retVal+=analogRead(data);
    delay(10);
  }
  digitalWrite(vcc, LOW);
  retVal /=  5;
  if(negate)
    return 100 - map(retVal, 0, 1024, 0, 100);
  else
    return map(retVal, 0, 1024, 0, 100);
}


